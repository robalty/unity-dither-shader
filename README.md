# PixelDither.shader

I was looking for something like this but couldn't find anything that really   
fit, so I figured I'd release what I made in case anybody else was looking for   
something similar. Basically, this is a shader that, when applied to the main   
camera, does two things: first, it reduces the pixel resolution of the render   
output, and then it quantizes the remaining colors to fit into the provided   
color palette. I've left as much room for configuration as possible, and I'll   
describe here what options you've got.   
 
### 'Y Resolution'
This option allows you to control how pixelated the scene is. To maintain an    
integer number of pixels on the screen, the number provided is multiplied by 9   
and used as the new height of the image - with a 16:9 render target, setting the   
'Y Resolution' option to 1 renders at 16x9 pixels.  

### 'Palette'
While my quantization method isn't perfect, I tinkered around with it a lot  
and found that it gave decent results with a range of different palettes.  
Any palette you care to use can be put into the Palette field as a 1xN pixel  
texture, where N is the number of colors. Be sure to set the filtering in Unity  
to "Point", or else your colors will be blended, and look odd! After you've  
changed your palette, be sure to update the 'Palette Size (Width)' parameter of  
the shader as well - otherwise your palette won't be used properly.  

### 'Dither'
The other configurable texture is the dither pattern to use. This is stored as  
a square texture, with each pixel having a red value corresponding to the dither  
value of that index. The included texture is based on a standard Bayer matrix,  
but you can use any dither pattern you like and it should work fine, as long as  
it uses the same format.  

### 'Dither Pattern Width'
The size of your dither pattern. Used for scaling the dither pattern to match   
the pixelization of the scene.  

### 'Dither Multiplier'
This controls the color error tolerance of the algorithm - lower values will  
use the second closest color more freely (more visible dithering), while higher  
values will prefer the closest color match. Going too far in either direction  
will make it so you can't see the dither pattern anymore, since the dithered  
image is made of the mixture of the two extremes. This is what you'll want to  
mess with first if your pattern looks weird. Normal values fall somewhere in   
the range of 0.5 - 2.


## Tips
1. This is a post effect shader! Don't apply it to an object's material. You'll   
  need to apply it to a material and then add "postshaderScript.cs" to your main   
  camera, with your newly made pixeldither material as that script's parameter.   
2. Before messing with anything else, check the dither multiplier! It can help!   
3. Be aware of the limitations of your palette! For instance, my naive 64 color   
  palette looks weird in the example picture with a white sphere, because the   
  palette only has white, two grays, and black, and has to interpolate with   
  other colors. Changing the colors in the scene makes it perform much better. 
4. If you actually want to see the dither pattern in the final image, use a  
   smaller dither pattern.
5. Generally speaking, larger palettes look better with higher multipliers.