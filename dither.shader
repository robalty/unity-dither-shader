﻿Shader "custom/dither"
{
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_PaletteTex ("Palette", 2D) = "black" {}
		_PaletteSize ("Palette Size (Width)", float) = 8
		_DitherTex ("Dither", 2D) = "black" {}
		_DitherMul ("Dither Multiplier", float) = 1
		
	}
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _DitherTex;
            uniform float4 _DitherTex_TexelSize;
            uniform float4 _MainTex_TexelSize;
			float _DitherMul;

            half indexValue(float2 uv) {
                float2 t = uv / (_MainTex_TexelSize/_DitherTex_TexelSize);
                fixed4 ret = tex2D(_DitherTex, t);
                return _DitherMul * half(ret.r);    
            }
			


            half hueDistance(fixed4 col1, fixed4 col2) {
                half tempr = half(col1.r) - half(col2.r);
                half tempg = half(col1.g) - half(col2.g);
                half tempb = half(col1.b) - half(col2.b); 
                return (abs(tempr) + abs(tempg) + abs(tempb))/3;
            }

            struct Colors{
                fixed4 col1;
                fixed4 col2;
            };

            uniform float4 _PaletteTex_TexelSize;
            sampler2D _PaletteTex;
			float _PaletteSize;
            
            Colors closestColors(fixed4 color) {
                Colors ret;
                fixed4 closest = fixed4(-2,-2,-2,1);
                fixed4 secondClosest = fixed4(-2,-2,-2,1);
                fixed4 temp;
                for (int i = 0; i < _PaletteSize; ++i) {
                    float2 tempCoord = float2(i + 0.5, 0.5);
                    float2 tempUV = tempCoord * _PaletteTex_TexelSize.xy;
                    temp = tex2D(_PaletteTex, tempUV);
                    half tempDistance = hueDistance(color, temp);
                    if (tempDistance < hueDistance(color, closest)){
                        secondClosest = closest;
                        closest = temp;
					} else if (tempDistance < hueDistance(secondClosest, color)) 
                        secondClosest = temp;
				}
                ret.col1 = closest;
                ret.col2 = secondClosest;
                return ret;
            }

            fixed4 dither(fixed4 color, half d) {
                Colors colorOptions = closestColors(color);
                fixed4 closestColor = colorOptions.col1;
                fixed4 secondClosestColor = colorOptions.col2;
                half hueDiff = hueDistance(color, closestColor);
                if(hueDiff < ((16 * d) / _PaletteSize))
                    return closestColor;
				return secondClosestColor;
            }

            sampler2D _MainTex;
			
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                half d = indexValue(i.uv);
                fixed4 finalPix = dither(col, d);
                return finalPix;
            }
            ENDCG
        }
    }
}
